package com.example.market.controller;

import com.example.market.dto.MarketRequest;
import com.example.market.dto.MarketResponse;
import com.example.market.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {
    private final MarketService marketService;

    //Write APIs for all



    @PostMapping
    public MarketResponse createMarket(@RequestBody @Valid MarketRequest request){
        return marketService.create(request);
    }

    @GetMapping("/{marketId}")
    public MarketResponse getMarket(@PathVariable Long marketId){
        return marketService.getMarket(marketId);
    }

    @GetMapping("/all")
    public List<MarketResponse> getMarket(){
        return marketService.getAllMarkets();
    }

    @DeleteMapping("/{marketId}")
    public void deleteMarketById(@PathVariable Long marketId){
        marketService.deleteById(marketId);
    }
    @PutMapping("/{marketId}/{branchId}")
    public MarketResponse updateMarket(@PathVariable Long marketId,
                                       @PathVariable Long branchId,
                                       @RequestBody MarketRequest marketRequest){
        return marketService.updateMarket(marketId, branchId, marketRequest);
    }

}
