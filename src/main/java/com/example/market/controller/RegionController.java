package com.example.market.controller;

import com.example.market.dto.RegionRequest;
import com.example.market.dto.RegionResponse;
import com.example.market.service.RegionService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/region")
public class RegionController {

    private final RegionService regionService;

    public RegionController(RegionService regionService) {
        this.regionService = regionService;
    }
    @DeleteMapping("/{regionId}")
    public void delete(@PathVariable Long regionId){
        regionService.delete(regionId);
    }

    @PostMapping("/{marketId}")
    public RegionResponse createRegion(@PathVariable long marketId, @RequestBody RegionRequest regionRequest){
        return regionService.createRegion(marketId,regionRequest);
    }
}
