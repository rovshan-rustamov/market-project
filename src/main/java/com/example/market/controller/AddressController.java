package com.example.market.controller;

import com.example.market.dto.AddressRequest;
import com.example.market.dto.AddressResponse;
import com.example.market.service.AddressService;
import com.example.market.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;

    @PostMapping("/{branchId}")
    public AddressResponse create(@PathVariable Long branchId, @RequestBody AddressRequest request){
        return addressService.create(request, branchId);
    }

    @PutMapping("/{branchId}/{addressId}")
    public AddressResponse updateAddress(@PathVariable Long branchId
                                         ,@PathVariable Long addressId
                                         ,@RequestBody AddressRequest addressRequest){
        return addressService.updateAddress(branchId, addressId,addressRequest);
    }

}
