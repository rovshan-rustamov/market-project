package com.example.market.controller;

import com.example.market.dto.BranchRequest;
import com.example.market.dto.BranchResponse;
import com.example.market.service.BranchService;
import com.example.market.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {
    public final BranchService branchService;

    //Write APIs for all


    @PostMapping("/{marketId}")
    public BranchResponse create(@PathVariable Long marketId, @RequestBody BranchRequest request){
        return branchService.create(marketId, request);
    }

    @PutMapping("/{marketId}/branch/{branchId}")
    public BranchResponse update(@PathVariable Long marketId,
                                 @PathVariable Long branchId,
                                 @RequestBody BranchRequest request){
        return branchService.update(marketId, branchId, request);
    }

    @DeleteMapping("/{branchId}")
    public String deleteBranch(@PathVariable Long branchId){
        return branchService.deleteBranch(branchId);
    }

    @GetMapping("/{branchId}")
    public BranchResponse getBranch(@PathVariable Long branchId){
        return branchService.getBranch(branchId);
    }
}
