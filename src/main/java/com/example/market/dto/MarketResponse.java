package com.example.market.dto;

import com.example.market.model.Branch;
import com.example.market.model.Region;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MarketResponse {
    Long id;
    String market;
    List<BranchResponse> branchList = new ArrayList<>();
    List<Region> regions = new ArrayList<>();

}
