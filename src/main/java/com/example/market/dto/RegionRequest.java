package com.example.market.dto;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegionRequest {
    String region;

//    @Enumerated(EnumType.STRING) to save enums in database
//    RegionType region;



}
