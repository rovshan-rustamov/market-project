package com.example.market.repository;

import com.example.market.model.Lesson;
import com.example.market.model.Student;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LessonRepository extends JpaRepository<Lesson, Long> {
    //JPQL
//    @Query(value = "select l from Lesson l join fetch l.students s")
//    List<Lesson> getAll();


//    @EntityGraph(value = "LessonsWithStudents")
//         @EntityGraph(type = EntityGraph.EntityGraphType.FETCH ,attributePaths = {"students","teachers"})
//    List<Lesson> findAllBy();
}
