package com.example.market.repository;

import com.example.market.model.Branch;
import com.example.market.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface MarketRepository extends JpaRepository<Market, Long> {
//    @Override
//    @Query("select m from Market m join fetch m.branchList b join fetch m.regions join fetch b.address a ")
//    List<Market> findAll();

    @Query(value = "select m from Market m join fetch m.regions r where r.id = :id")
    Collection<Market> findAllByRegionId(Long id);
    @Query("select m from Market m join fetch m.branchList b join fetch m.regions r join fetch b.address a")
    List<Market> findAll();
}
