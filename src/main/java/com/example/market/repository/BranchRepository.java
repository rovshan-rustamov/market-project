package com.example.market.repository;

import com.example.market.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BranchRepository extends JpaRepository<Branch, Long> {
    // SQL @Query(value = "select * from branch where market_id = :id")
    //JPQL @Query(value = "select p from Branch p where p.market.id = :id")
//    List<Branch> getFromDB(Long id);

}
