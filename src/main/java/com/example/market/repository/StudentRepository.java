package com.example.market.repository;

import com.example.market.model.Region;
import com.example.market.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
