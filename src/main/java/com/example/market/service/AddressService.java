package com.example.market.service;

import com.example.market.dto.AddressRequest;
import com.example.market.dto.AddressResponse;
import com.example.market.model.Address;
import com.example.market.model.Branch;
import com.example.market.repository.AddressRepository;
import com.example.market.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AddressService {
    private final AddressRepository addressRepository;
    private final BranchRepository branchRepository;
    private final ModelMapper modelMapper;
    public AddressResponse create(AddressRequest request, Long branchId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException( String.format("no such branch id = %s",branchId)));
        Address address = modelMapper.map(request, Address.class);
        branch.setAddress(address);
        address.setBranch(branch);
        addressRepository.save(address);
        branchRepository.save(branch);
        return modelMapper.map(address, AddressResponse.class);
    }

    public AddressResponse updateAddress(Long branchId, Long addressId, AddressRequest addressRequest) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("No such branch id: %s", branchId)));

        Optional<Address> address = addressRepository.findById(addressId);
        address.ifPresent(address1 -> {
            address1.setAddress(addressRequest.getAddress());

            branchRepository.save(branch);
        });

        return modelMapper.map(address, AddressResponse.class);
    }


}
