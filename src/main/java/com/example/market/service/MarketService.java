package com.example.market.service;

import com.example.market.dto.MarketRequest;
import com.example.market.dto.MarketResponse;
import com.example.market.model.Market;
import com.example.market.repository.MarketRepository;
import jakarta.transaction.Transactional;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.error.Mark;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@Data
public class MarketService {
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;

    public MarketResponse create(MarketRequest marketRequest) {
        Market market = modelMapper.map(marketRequest, Market.class);
        marketRepository.save(market);
        return modelMapper.map(market, MarketResponse.class);

    }
    @Transactional
    public MarketResponse getMarket(Long marketId) {
        Market market = marketRepository.findById(marketId).
                orElseThrow(() -> new RuntimeException(String.format("Market with %s id not found", marketId)));

        return modelMapper.map(market, MarketResponse.class);
    }
    @Transactional
    public List<MarketResponse> getAllMarkets() {
        List<Market> markets = marketRepository.findAll();
        List<MarketResponse> marketResponses = new LinkedList<>();
        for (int i= 0; i < markets.size(); i++){
            marketResponses.add(modelMapper.map(markets.get(i),MarketResponse.class));
        }

        return marketResponses;
    }

    public void deleteById(Long marketId) {
        marketRepository.deleteById(marketId);
    }

    public MarketResponse updateMarket(Long marketId, Long branchId, MarketRequest marketRequest) {
        Optional<Market> market = marketRepository.findById(marketId);
        market.ifPresent(market1 -> {
            market1.setMarket(marketRequest.getMarket());
            marketRepository.save(market1);
        });

        return modelMapper.map(market,MarketResponse.class);
    }
}
