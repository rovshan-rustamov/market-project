package com.example.market.service;

import com.example.market.dto.RegionRequest;
import com.example.market.dto.RegionResponse;
import com.example.market.model.Market;
import com.example.market.model.Region;
import com.example.market.repository.MarketRepository;
import com.example.market.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RegionService {
    private final RegionRepository regionRepository;
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;



    public void delete(Long regionId){
        Collection<Market> markets = marketRepository.findAllByRegionId(regionId);
        Region region = regionRepository.findById(regionId).orElseThrow(RuntimeException::new);
        markets.forEach(market -> {
            market.getRegions().remove(region);
        });

        marketRepository.saveAll(markets);
        regionRepository.delete(region);
    }

    public RegionResponse createRegion(long marketId, RegionRequest regionRequest) {
        Market market = marketRepository.findById(marketId).orElseThrow(() -> new RuntimeException());
        Region region = modelMapper.map(regionRequest, Region.class);
        market.getRegions().add(region);
        regionRepository.save(region);
        marketRepository.save(market);
        return modelMapper.map(region, RegionResponse.class);
    }
}
