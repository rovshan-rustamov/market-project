package com.example.market.service;

import com.example.market.dto.BranchRequest;
import com.example.market.dto.BranchResponse;
import com.example.market.model.Branch;
import com.example.market.model.Market;
import com.example.market.repository.BranchRepository;
import com.example.market.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BranchService {
    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;

    public BranchResponse create(Long marketId, BranchRequest request) {
        Market market = marketRepository.findById(marketId).
                orElseThrow(() -> new RuntimeException(String.format("Market with %s id not found", marketId)));
        Branch branch = modelMapper.map(request, Branch.class);
        branch.setMarket(market);
        market.getBranchList().add(branch);
        branchRepository.save(branch);
        marketRepository.save(market);
        return modelMapper.map(branch, BranchResponse.class);
    }

    public BranchResponse update(Long marketId, Long branchId, BranchRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("No such market %s" , marketId)));
        Optional<Branch> branch = branchRepository.findById(branchId);
        branch.ifPresent(branch1 -> {
            branch1.setBranch(request.getBranch());
            branchRepository.save(branch1);
        });
        return modelMapper.map(branch, BranchResponse.class);
    }

    public String deleteBranch(Long branchId) {
        Branch branch = branchRepository.findById(branchId).orElseThrow(()->new RuntimeException("nooo"));
        branchRepository.deleteById(branchId);
        return "deleted";
    }

    public BranchResponse getBranch(Long branchId) {
        Branch branch = branchRepository.findById(branchId).get();
        return modelMapper.map(branch, BranchResponse.class);
    }
}
