package com.example.market.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@NamedQueries(
        @NamedQuery(name = "findBranchesByMarketId", query = "select p from Branch p where p.market.id = :d")
)
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    private String branch;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    Address address;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.PERSIST)
    @ToString.Exclude
    Market market;


}
