package com.example.market.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString
@NamedEntityGraph(
        name = "LessonsWithStudents",
        attributeNodes = {
                @NamedAttributeNode("students"),
        }
)
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;


    String lesson;

    @OneToMany(mappedBy = "lesson", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @Builder.Default
    Set<Student> students  = new HashSet<>();

}
