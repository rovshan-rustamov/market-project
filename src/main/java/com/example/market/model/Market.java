package com.example.market.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@ToString
public class Market {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;


    String market;

    @OneToMany(mappedBy = "market", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @Builder.Default
    Set<Branch> branchList = new HashSet<>();

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER) // Never All it will delete all of the relations
    @JoinTable(name = "market_region",
            joinColumns = @JoinColumn(name = "market_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "region_id", referencedColumnName = "id"))
    Set<Region> regions = new HashSet<>();

}
