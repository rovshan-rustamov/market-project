package com.example.market;

import com.example.market.model.*;
import com.example.market.repository.*;
import lombok.Builder;
import com.example.market.service.RegionService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class MarketApplication implements CommandLineRunner {
    private final AddressRepository addressRepository;
    private final MarketRepository marketRepository;
    private final BranchRepository branchRepository;
    private final RegionService regionService;
    private final EntityManagerFactory emf;
    private final StudentRepository studentRepository;
    private final LessonRepository lessonRepository;

    public static void main(String[] args) {
        SpringApplication.run(MarketApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        System.out.println(lessonRepository.findAll());
//
////        branchRepository.getFromDB(2L).forEach(System.out::println);
//
//
////        em.getTransaction().begin();
////        List<Branch> branches = em.createNativeQuery("select * from branch where market_id= :id", Branch.class)
////                .setParameter("id",2)
////                .getResultList();
////        System.out.println(marketRepository.findById(2L).get());
//
//        Market bravo = marketRepository.findById(1L).get();
//        Region xetai  = Region.builder()
//                .region("Abseron")
//                .build();
//        Region nizami = Region.builder()
//                .region("Merkez")
//                .build();
//        bravo.getRegions().add(xetai);
//        bravo.getRegions().add(nizami);
//        Market market = Market.builder()
//                .market("Bizim Market")
//                .build();
//        Branch korogluBranch = Branch.builder()
//                .branch("Genclik")
//                .market(market)
//                .build();
//        Branch ahmadliBranch = Branch.builder()
//                .branch("Quba")
//                .market(market)
//                .build();
//        market.getBranchList().add(korogluBranch);
//        market.getBranchList().add(ahmadliBranch);
//        Address address1 = Address.builder()
//                .address("Ramiz 1234")
//                .build();
//        Address address2 = Address.builder()
//                .address("Koroglu Metrosu")
//                .build();
//        ahmadliBranch.setAddress(address1);
//        korogluBranch.setAddress(address2);
//        address1.setBranch(ahmadliBranch);
//        address2.setBranch(korogluBranch);
//
//        marketRepository.save(market);
//        marketRepository.findAll().forEach(System.out::println);
//
//        Branch branch = Branch.builder()
//                .branch("Ahmedli12")
//                .market(market)
//                .build();
//        branchRepository.save(branch);
//        Address address = Address.builder()
//                .address("Ramiz Quliyev 1")
//                .branch(branch)
//                .build();
//        addressRepository.save(address);
//        System.out.println(marketRepository.findAll());
//        Lesson lesson = Lesson.builder()
//                .lesson("Math")
//                .build();
//        for (int i = 0; i < 50; i++){
//            Student student = Student.builder()
//                    .name("Rovshan " + i)
//                    .surname("Rustamov " + i)
//                    .lesson(lesson)
//                    .build();
//
//            lesson.getStudens().add(student);
//        }
//        Lesson lesson2 = Lesson.builder()
//                .lesson("Biology")
//
//                .build();
//
//        for (int i = 0; i < 50; i++){
//            Student student = Student.builder()
//                    .name("Omar " + i)
//                    .surname("Ziyad " + i)
//                    .lesson(lesson2)
//                    .build();
//
//            lesson2.getStudens().add(student);
//        }
//        lessonRepository.save(lesson);
//        lessonRepository.save(lesson2);

    }
}
